<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="custom.css">
    <meta charset="UTF-8">
    <title>Contact Form</title>
</head>
<body>
<h2>Contact Form</h2>
<div class="row">
    <form name="contact" id="contact" method="post" action="project1.php" class="col-lg-12">
        <div class="col-lg-12">
            <input name="name" type="text" placeholder="Your Full Name" class="form-control" data-error="That email address is invalid" required>
        </div>
        <div class="col-lg-12">
            <input name="email" id="email" type="email" placeholder="Your Email" class="form-control" data-error="That email address is invalid" required>
        </div>
        <div class="col-lg-12">
            <textarea name="question" data-error="Please mention your question" class="form-control" placeholder="Ask Your Question Here" required></textarea>
        </div>
        <div class="col-lg-12">
            <input class="butt" type="submit" name="Submit" value="Submit">
        </div>
    </form>
</div>
<div class="intro"><div class="col-lg-4 btn-primary"><a href="/">Back to Home Page</a></div></div>
</body>
</html>