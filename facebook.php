<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Photos with Friends!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="custom.css">
    <script>
        /**
         * This is the getPhoto library
         */
        function makeFacebookPhotoURL( id, accessToken ) {
            return 'https://graph.facebook.com/' + id + '/picture?access_token=' + accessToken;
        }
        function logout(){
            FB.logout(function(response) {
                var butt = '<input type="button" onclick="getPhotosAfterLogin();" value="Login to Facebook"/>';
                $(".welcome").html(butt);

                setTimeout(function(){
                    location.reload();
                    }, 1000);
            });
        }
        function login( callback ) {
            FB.login(function(response) {
                if (response.authResponse) {
                    //console.log('Welcome!  Fetching your information.... ');
                    var butt = "<input type='button' onclick='logout();' value='Logout from Facebook'/>";
                    $(".welcome").html(butt);
                    if (callback) {
                        callback(response);
                    }
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                    var butt = '<input type="button" onclick="getPhotosAfterLogin();" value="Login to Facebook"/>';
                    $(".welcome").html(butt);
                }
            },{scope: 'user_photos'} );
        }
        function getAlbums( callback ) {
            FB.api(
                '/me/albums',
                {fields: 'id,cover_photo'},
                function(albumResponse) {
                    //console.log( ' got albums ' );
                    if (callback) {
                        callback(albumResponse);
                    }
                }
            );
        }
        function getPhotosForAlbumId( albumId, callback ) {
            FB.api(
                '/'+albumId+'/photos',
                {fields: 'id'},
                function(albumPhotosResponse) {
                    //console.log( ' got photos for album ' + albumId );
                    if (callback) {
                        callback( albumId, albumPhotosResponse );
                    }
                }
            );
        }

        function getPhotos(callback) {
            var allPhotos = [];
            var accessToken = '';
            login(function(loginResponse) {
                accessToken = loginResponse.authResponse.accessToken || '';
                getAlbums(function(albumResponse) {
                    var i, album, deferreds = {}, listOfDeferreds = [];
                    for (i = 0; i < albumResponse.data.length; i++) {
                        album = albumResponse.data[i];
                        deferreds[album.id] = $.Deferred();
                        listOfDeferreds.push( deferreds[album.id] );
                        getPhotosForAlbumId( album.id, function( albumId, albumPhotosResponse ) {
                            var i, facebookPhoto;
                            for (i = 0; i < albumPhotosResponse.data.length; i++) {
                                facebookPhoto = albumPhotosResponse.data[i];
                                allPhotos.push({
                                    'id'	:	facebookPhoto.id,
                                    'added'	:	facebookPhoto.created_time,
                                    'url'	:	makeFacebookPhotoURL( facebookPhoto.id, accessToken )
                                });
                            }
                            deferreds[albumId].resolve();
                        });
                    }
                    $.when.apply($, listOfDeferreds ).then( function() {
                        if (callback) {
                            callback( allPhotos );
                        }
                    }, function( error ) {
                        if (callback) {
                            callback( allPhotos, error );
                        }
                    });
                });
            });
        }
    </script>

    <script>
        /**
         * This is the bootstrap / app script
         */
            // wait for DOM and facebook auth
        var docReady = $.Deferred();
        var facebookReady = $.Deferred();
        $(document).ready(docReady.resolve);
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1158668804277618',
                status     : true,
                cookie     : true,
                xfbml      : true
            });
            facebookReady.resolve();
        };
        $.when(docReady, facebookReady).then(function() {
            if (typeof getPhotos !== 'undefined') {

            }
        });
        function getPhotosAfterLogin(){
            getPhotos( function( photos ) {
                for(var i=0; i<photos.length; i++){
                    photo = photos[i];
                    var div = $(document.createElement("div"));
                    div.attr('class','fb-img');
                    var img = $(document.createElement('img'));
                    img.attr('src', photo.url);
                    $(div).append(img);
                    $( "#imagediv" ).append( div );
                }
            });
        }
        // call facebook script
        (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "http://connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
        }(document));
    </script>
</head>
<body>
<div id="fb-root"></div>
<div class="intro"><div class="col-lg-4 btn-primary"><a href="/">Back to Home Page</a></div></div>
<div class="welcome"><input type="button" onclick="getPhotosAfterLogin();" value="Login to Facebook"/></div>
<div id="imagediv"></div>
</body>
</html>